import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Home } from './pages/Home';
import { MedicoForm } from './components/medico/medico_form';
import { HomeNavbar } from './components/navbar/navbar';
import { Login } from './pages/login';
import { Route, Routes } from 'react-router-dom';
import { ListMedico } from './components/medico/list_medico';
import { EditarMedico } from './components/medico/edit_medico';
import { ListPaciente } from './components/paciente/list_paciente';
import { PacienteForm } from './components/paciente/paciente_form';
import { EditarPaciente } from './components/paciente/edit_paciente';
import { Banner } from './components/banner/Banner';
import { Footer } from './components/footer/footer';
import { ListHorario } from './components/horarios/list_horario';
import { HorarioForm } from './components/horarios/add_horario';
import { EditarHorario } from './components/horarios/edit_horario';

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className='App'>
      <HomeNavbar />
      <Routes>
        <Route path='/' element={<Banner />} />
        <Route path='/login' element={<Login />} />
        <Route path='/add_medico' element={<MedicoForm />} />
        <Route path='/list_medico' element={<ListMedico />} />
        <Route path='/editar_medico/:id_medico' element={<EditarMedico />} />
        <Route path='/add_paciente' element={<PacienteForm />} />
        <Route path='/list_paciente' element={<ListPaciente />} />
        <Route
          path='/editar_paciente/:id_paciente'
          element={<EditarPaciente />}
        />
        <Route path='/add_horario' element={<HorarioForm />} />
        <Route path='/list_horario' element={<ListHorario />} />
        <Route path='/editar_horario/:id_horario' element={<EditarHorario />} />
      </Routes>
      <Footer />
      {/* <MedicoForm /> */}
    </div>
  );
}

export default App;
