import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

export function Login() {
  const navigate = useNavigate();
  function uLogin(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios.post('http://localhost:8000/api/token/', values).then((respuesta) => {
      console.log(respuesta);
      localStorage.setItem('token', respuesta.data.access);
      navigate('/');
    });
  }
  return (
    <div className='container'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          uLogin(e);
        }}
      >
        <FormGroup>
          <Label for='Username'>Username</Label>
          <Input
            id='username'
            name='username'
            placeholder='with a placeholder'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='Password'>Password</Label>
          <Input
            id='Password'
            name='password'
            placeholder='password placeholder'
            type='password'
          />
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    </div>
  );
}
