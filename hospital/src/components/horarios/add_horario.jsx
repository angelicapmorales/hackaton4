import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

export function HorarioForm() {
  const navigate = useNavigate();
  let [horarios, setHorarios] = useState([]);

  function uHorario(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());
    let token = localStorage.getItem('token');

    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };

    axios
      .post('http://127.0.0.1:8000/api/hospital/horarios/', values, config)
      .then((respuesta) => {
        console.log(respuesta);
        setHorarios(respuesta.data);
        navigate('/list_horario');
      });
  }
  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          uHorario(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Medico</Label>
          <Input
            id='Nombre'
            name='medico'
            placeholder='Escriba el id del Medico'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='fecha_atencion'>Fecha Atencion</Label>
          <Input
            id='fecha_atencion'
            name='fecha_atencion'
            placeholder='Fecha de atencion'
            type='date'
          />
        </FormGroup>
        <FormGroup>
          <Label for='inicio_atencion'>Inicio de Atencion</Label>
          <Input
            id='inicio_atencion'
            name='inicio_atencion'
            placeholder='Inicio de atencion'
            type='time'
          />
        </FormGroup>
        <FormGroup>
          <Label for='fin_atencion'>Fin Atencion</Label>
          <Input
            id='fin_atencion'
            name='fin_atencion'
            placeholder='fin de atencion'
            type='time'
          />
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input id='Activo' name='activo' type='boolean'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='usuario_registro'>Usuario Registro</Label>
          <Input
            id='usuario_registro'
            name='usuario_registro'
            placeholder='Usuario registro'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='usuario_modificacion'>Usuario Modificacion</Label>
          <Input
            id='usuario_modificacion'
            name='usuario_modificacion'
            placeholder='Usuario Modificacion'
            type='text'
          />
        </FormGroup>

        <Button>Guardar</Button>
      </Form>
    </div>
  );
}
