import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';

export function EditarHorario() {
  let [horario, setHorario] = useState([]);
  let parametrosUrl = useParams();
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const navigate = useNavigate();
  function editHorario(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .put(
        `http://127.0.0.1:8000/api/hospital/horarios/${parametrosUrl.id_horario}/`,
        values,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/list_horario');
      });
  }
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/hospital/horarios/${parametrosUrl.id_horario}`,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        setHorario(respuesta.data);
      });
  }, []);

  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          editHorario(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Medico</Label>
          <Input
            id='Nombre'
            name='medico'
            placeholder='Escriba el id del Medico'
            type='text'
            defaultValue={horario.medico}
          />
        </FormGroup>
        <FormGroup>
          <Label for='fecha_atencion'>Fecha Atencion</Label>
          <Input
            id='fecha_atencion'
            name='fecha_atencion'
            placeholder='Fecha de atencion'
            type='date'
            defaultValue={horario.fecha_atencion}
          />
        </FormGroup>
        <FormGroup>
          <Label for='inicio_atencion'>Inicio de Atencion</Label>
          <Input
            id='inicio_atencion'
            name='inicio_atencion'
            placeholder='Inicio de atencion'
            type='time'
            defaultValue={horario.inicio_atencion}
          />
        </FormGroup>
        <FormGroup>
          <Label for='fin_atencion'>Fin Atencion</Label>
          <Input
            id='fin_atencion'
            name='fin_atencion'
            placeholder='fin de atencion'
            type='time'
            defaultValue={horario.fin_atencion}
          />
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input
            id='Activo'
            name='activo'
            type='boolean'
            defaultValue={horario.activo}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='usuario_registro'>Usuario Registro</Label>
          <Input
            id='usuario_registro'
            name='usuario_registro'
            placeholder='Usuario registro'
            type='text'
            defaultValue={horario.usuario_registro}
          />
        </FormGroup>
        <FormGroup>
          <Label for='usuario_modificacion'>Usuario Modificacion</Label>
          <Input
            id='usuario_modificacion'
            name='usuario_modificacion'
            placeholder='Usuario Modificacion'
            type='text'
            defaultValue={horario.usuario_modificacion}
          />
        </FormGroup>

        <Button>Guardar</Button>
      </Form>
    </div>
  );
}
