import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table } from 'reactstrap';
import axios from 'axios';
import { HorarioItem } from './horario_item';

export function ListHorario() {
  const navigate = useNavigate();
  let [horarios, setHorarios] = useState([]);
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/hospital/horarios/', config)
      .then((respuesta) => {
        console.log(respuesta);
        setHorarios(respuesta.data);
        navigate('/list_horario');
      });
  }, []);
  return (
    <div className='container py-5'>
      <Table bordered responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Medico</th>
            <th>Fecha Atencion</th>
            <th>Inicio Atencion</th>
            <th>Fin Atencion</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          {horarios
            ? horarios.map((horario) => {
                return <HorarioItem {...horario} key={horario.id} />;
              })
            : 'Cargando'}
        </tbody>
      </Table>
    </div>
  );
}
