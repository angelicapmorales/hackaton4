import { Link } from 'react-router-dom';
import axios from 'axios';

export function HorarioItem(horario) {
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  function deleteHorario() {
    axios
      .delete(
        `http://127.0.0.1:8000/api/hospital/horarios/${horario.id}`,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }
  return (
    <tr>
      <th scope='row'>{horario.id}</th>
      <td>{horario.medico}</td>
      <td>{horario.fecha_atencion}</td>
      <td>{horario.inicio_atencion}</td>
      <td>{horario.fin_atencion}</td>
      <td>
        <Link
          className='btn btn-secondary mx-3'
          to={`/editar_horario/${horario.id}`}
        >
          Editar
        </Link>

        <button
          className='btn btn-info'
          type='submit'
          onClick={() => {
            deleteHorario();
          }}
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
}
