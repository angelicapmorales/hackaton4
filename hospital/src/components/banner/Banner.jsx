import hospital1 from '../../asset/img/hospital1.png';

export function Banner() {
  return (
    <div>
      <img
        style={{ height: '740px', objectFit: 'cover' }}
        src={hospital1}
        className='card-img py-5'
        alt='banner'
      />
    </div>
  );
}
