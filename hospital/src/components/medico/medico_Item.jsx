import { Link } from 'react-router-dom';
import axios from 'axios';

export function MedicoItem(medico) {
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  function deleteMedico() {
    axios
      .delete(`http://127.0.0.1:8000/api/hospital/medico/${medico.id}`, config)
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }
  return (
    <tr>
      <th scope='row'>{medico.id}</th>
      <td>{medico.nombres}</td>
      <td>{medico.apellidos}</td>
      <td>{medico.dni}</td>
      <td>{medico.fecha_nacimiento}</td>
      <td>{medico.telefono}</td>
      <td>
        <Link
          className='btn btn-secondary mx-3'
          to={`/editar_medico/${medico.id}`}
        >
          Editar
        </Link>

        <button
          className='btn btn-info'
          type='submit'
          onClick={() => {
            deleteMedico();
          }}
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
}
