import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';

export function EditarMedico() {
  let [medico, setMedico] = useState([]);
  let parametrosUrl = useParams();
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const navigate = useNavigate();
  function editMedico(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .put(
        `http://127.0.0.1:8000/api/hospital/medico/${parametrosUrl.id_medico}/`,
        values,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/list_medico');
      });
  }
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/hospital/medico/${parametrosUrl.id_medico}`,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        setMedico(respuesta.data);
      });
  }, []);

  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          editMedico(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Nombres</Label>
          <Input
            id='Nombre'
            name='nombres'
            placeholder='Escriba su nombre'
            type='text'
            defaultValue={medico.nombres}
          />
        </FormGroup>
        <FormGroup>
          <Label for='Apellidos'>Apellidos</Label>
          <Input
            id='Apellidos'
            name='apellidos'
            placeholder='Escriba sus apellidos'
            type='text'
            defaultValue={medico.apellidos}
          />
        </FormGroup>
        <FormGroup>
          <Label for='DNI'>DNI</Label>
          <Input
            id='DNI'
            name='dni'
            type='int'
            defaultValue={medico.dni}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Direccion'>Direccion</Label>
          <Input
            id='Direccion'
            name='direccion'
            type='text'
            defaultValue={medico.direccion}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Correo'>Correo</Label>
          <Input
            id='Correo'
            name='correo'
            type='email'
            defaultValue={medico.correo}
          />
        </FormGroup>
        <FormGroup>
          <Label for='Telefono'>Telefono</Label>
          <Input
            id='Telefono'
            name='telefono'
            type='int'
            defaultValue={medico.telefono}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Sexo'>Sexo</Label>
          <Input
            id='Sexo'
            name='sexo'
            type='text'
            defaultValue={medico.sexo}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Num_Colegiatura'>Numero Colegiatura</Label>
          <Input
            id='Num_Colegiatura'
            name='num_colegiatura'
            type='int'
            defaultValue={medico.num_colegiatura}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Fecha_nacimiento'>Fecha Nacimiento</Label>
          <Input
            id='Fecha_nacimiento'
            name='fecha_nacimiento'
            type='date'
            defaultValue={medico.fecha_nacimiento}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input
            id='Activo'
            name='activo'
            type='boolean'
            defaultValue={medico.activo}
          ></Input>
        </FormGroup>
        <Button>Guardar</Button>
      </Form>
    </div>
  );
}
