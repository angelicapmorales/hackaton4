import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

export function MedicoForm() {
  const navigate = useNavigate();
  let [medicos, setMedicos] = useState([]);

  function uMedico(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());
    let token = localStorage.getItem('token');

    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };

    axios
      .post('http://127.0.0.1:8000/api/hospital/medico/', values, config)
      .then((respuesta) => {
        console.log(respuesta);
        setMedicos(respuesta.data);
        navigate('/list_medico');
      });
  }
  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          uMedico(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Nombres</Label>
          <Input
            id='Nombre'
            name='nombres'
            placeholder='Escriba su nombre'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='Apellidos'>Apellidos</Label>
          <Input
            id='Apellidos'
            name='apellidos'
            placeholder='Escriba sus apellidos'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='DNI'>DNI</Label>
          <Input id='DNI' name='dni' type='int'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Direccion'>Direccion</Label>
          <Input id='Direccion' name='direccion' type='text'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Correo'>Correo</Label>
          <Input id='Correo' name='correo' type='email' />
        </FormGroup>
        <FormGroup>
          <Label for='Telefono'>Telefono</Label>
          <Input id='Telefono' name='telefono' type='int'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Sexo'>Sexo</Label>
          <Input id='Sexo' name='sexo' type='text'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Num_Colegiatura'>Numero Colegiatura</Label>
          <Input id='Num_Colegiatura' name='num_colegiatura' type='int'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Fecha_nacimiento'>Fecha Nacimiento</Label>
          <Input
            id='Fecha_nacimiento'
            name='fecha_nacimiento'
            type='date'
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input id='Activo' name='activo' type='boolean'></Input>
        </FormGroup>
        <Button>Agregar</Button>
      </Form>
    </div>
  );
}
