import { useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { MedicoItem } from './medico_Item';

export function ListMedico() {
  const navigate = useNavigate();
  let [medicos, setMedicos] = useState([]);
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/hospital/medico/', config)
      .then((respuesta) => {
        console.log(respuesta);
        setMedicos(respuesta.data);
        navigate('/list_medico');
      });
  }, []);
  return (
    <div className='container py-5'>
      <Table bordered responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>DNI</th>
            <th>Fecha Nacimiento</th>
            <th>Celular</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          {medicos
            ? medicos.map((medico) => {
                return <MedicoItem {...medico} key={medico.id} />;
              })
            : 'Cargando'}
        </tbody>
      </Table>
    </div>
  );
}
