import { Link } from 'react-router-dom';
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  NavLink,
} from 'reactstrap';

export function HomeNavbar() {
  return (
    <div>
      <Navbar color='info' dark expand='md'>
        <NavbarBrand href='/'>Hospital</NavbarBrand>
        <NavbarToggler onClick={function noRefCheck() {}} />
        <Collapse navbar>
          <Nav className='me-auto' navbar>
            <UncontrolledDropdown inNavbar nav>
              <DropdownToggle caret nav>
                Medico
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink className='text-dark' href='/add_medico'>
                    Agregar
                  </NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink className='text-dark' href='/list_medico'>
                    Listar
                  </NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown inNavbar nav>
              <DropdownToggle caret nav>
                Paciente
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink className='text-dark' href='/add_paciente'>
                    Agregar
                  </NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink className='text-dark' href='/list_paciente'>
                    Listar
                  </NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown inNavbar nav>
              <DropdownToggle caret nav>
                Citas
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>Agregar</DropdownItem>
                <DropdownItem>Listar</DropdownItem>
                <DropdownItem>Eliminar</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown inNavbar nav>
              <DropdownToggle caret nav>
                Horarios
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink className='text-dark' href='/add_horario'>
                    Agregar
                  </NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href='/list_horario' className='text-dark'>
                    Listar
                  </NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <NavLink href='/login'>Login</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
