import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table } from 'reactstrap';
import axios from 'axios';
import { PacienteItem } from './paciente_item';

export function ListPaciente() {
  const navigate = useNavigate();
  let [pacientes, setPacientes] = useState([]);
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/hospital/paciente/', config)
      .then((respuesta) => {
        console.log(respuesta);
        setPacientes(respuesta.data);
        navigate('/list_paciente');
      });
  }, []);
  return (
    <div className='container py-5'>
      <Table bordered responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>DNI</th>
            <th>Fecha Nacimiento</th>
            <th>Celular</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          {pacientes
            ? pacientes.map((paciente) => {
                return <PacienteItem {...paciente} key={paciente.id} />;
              })
            : 'Cargando'}
        </tbody>
      </Table>
    </div>
  );
}
