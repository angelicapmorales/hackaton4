import { Link } from 'react-router-dom';
import axios from 'axios';

export function PacienteItem(paciente) {
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  function deletePaciente() {
    axios
      .delete(
        `http://127.0.0.1:8000/api/hospital/paciente/${paciente.id}`,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }
  return (
    <tr>
      <th scope='row'>{paciente.id}</th>
      <td>{paciente.nombres}</td>
      <td>{paciente.apellidos}</td>
      <td>{paciente.dni}</td>
      <td>{paciente.fecha_nacimiento}</td>
      <td>{paciente.telefono}</td>
      <td>
        <Link
          className='btn btn-secondary mx-3'
          to={`/editar_paciente/${paciente.id}`}
        >
          Editar
        </Link>

        <button
          className='btn btn-info'
          type='submit'
          onClick={() => {
            deletePaciente();
          }}
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
}
