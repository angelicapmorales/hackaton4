import { useNavigate } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';
import { useState } from 'react';

export function PacienteForm() {
  const navigate = useNavigate();
  let [pacientes, setPacientes] = useState([]);

  function uPaciente(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());
    let token = localStorage.getItem('token');

    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };

    axios
      .post('http://127.0.0.1:8000/api/hospital/paciente/', values, config)
      .then((respuesta) => {
        console.log(respuesta);
        setPacientes(respuesta.data);
        navigate('/list_paciente');
      });
  }
  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          uPaciente(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Nombres</Label>
          <Input
            id='Nombre'
            name='nombres'
            placeholder='Escriba su nombre'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='Apellidos'>Apellidos</Label>
          <Input
            id='Apellidos'
            name='apellidos'
            placeholder='Escriba sus apellidos'
            type='text'
          />
        </FormGroup>
        <FormGroup>
          <Label for='DNI'>DNI</Label>
          <Input id='DNI' name='dni' type='int'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Direccion'>Direccion</Label>
          <Input id='Direccion' name='direccion' type='text'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Telefono'>Telefono</Label>
          <Input id='Telefono' name='telefono' type='int'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Sexo'>Sexo</Label>
          <Input id='Sexo' name='sexo' type='text'></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Fecha_nacimiento'>Fecha Nacimiento</Label>
          <Input
            id='Fecha_nacimiento'
            name='fecha_nacimiento'
            type='date'
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input id='Activo' name='activo' type='boolean'></Input>
        </FormGroup>
        <Button>Agregar</Button>
      </Form>
    </div>
  );
}
