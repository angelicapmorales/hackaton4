import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';

export function EditarPaciente() {
  let [paciente, setPaciente] = useState([]);
  let parametrosUrl = useParams();
  let token = localStorage.getItem('token');

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const navigate = useNavigate();
  function editPaciente(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .put(
        `http://127.0.0.1:8000/api/hospital/paciente/${parametrosUrl.id_paciente}/`,
        values,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/list_paciente');
      });
  }
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/hospital/paciente/${parametrosUrl.id_paciente}`,
        config
      )
      .then((respuesta) => {
        console.log(respuesta);
        setPaciente(respuesta.data);
      });
  }, []);

  return (
    <div className='container py-5'>
      <Form
        onSubmit={(e) => {
          console.log(e);
          editPaciente(e);
        }}
      >
        <FormGroup>
          <Label for='Nombre'>Nombres</Label>
          <Input
            id='Nombre'
            name='nombres'
            placeholder='Escriba su nombre'
            type='text'
            defaultValue={paciente.nombres}
          />
        </FormGroup>
        <FormGroup>
          <Label for='Apellidos'>Apellidos</Label>
          <Input
            id='Apellidos'
            name='apellidos'
            placeholder='Escriba sus apellidos'
            type='text'
            defaultValue={paciente.apellidos}
          />
        </FormGroup>
        <FormGroup>
          <Label for='DNI'>DNI</Label>
          <Input
            id='DNI'
            name='dni'
            type='int'
            defaultValue={paciente.dni}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Direccion'>Direccion</Label>
          <Input
            id='Direccion'
            name='direccion'
            type='text'
            defaultValue={paciente.direccion}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Telefono'>Telefono</Label>
          <Input
            id='Telefono'
            name='telefono'
            type='int'
            defaultValue={paciente.telefono}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Sexo'>Sexo</Label>
          <Input
            id='Sexo'
            name='sexo'
            type='text'
            defaultValue={paciente.sexo}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Fecha_nacimiento'>Fecha Nacimiento</Label>
          <Input
            id='Fecha_nacimiento'
            name='fecha_nacimiento'
            type='date'
            defaultValue={paciente.fecha_nacimiento}
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label for='Activo'>Activo</Label>
          <Input
            id='Activo'
            name='activo'
            type='boolean'
            defaultValue={paciente.activo}
          ></Input>
        </FormGroup>
        <Button>Guardar</Button>
      </Form>
    </div>
  );
}
