# Generated by Django 4.0.5 on 2022-06-10 02:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hospital', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medico',
            name='activo',
            field=models.BooleanField(default=False),
        ),
    ]
