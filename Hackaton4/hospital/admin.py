from django.contrib import admin

# Register your models here.
from hospital.models import Especialidad, MedicosEspecialidad, Horario, Cita, Medico, Paciente

admin.site.register(Medico)
admin.site.register(Paciente)
admin.site.register(Especialidad)
admin.site.register(MedicosEspecialidad)
admin.site.register(Horario)
admin.site.register(Cita)
