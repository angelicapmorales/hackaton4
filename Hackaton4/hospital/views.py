from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, permissions

from hospital.models import Medico, Paciente, Especialidad, MedicosEspecialidad, Horario, Cita
from hospital.serializer import MedicoSerializer, PacienteSerializer, EspecialidadSerializer, \
    MedicosEspecialidadesSerializer, HorarioSerializer, CitaSerializer


class MedicoViewSet(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer
    permission_classes = [permissions.IsAuthenticated]


class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer
    permission_classes = [permissions.IsAuthenticated]


class EspecialidadViewSet(viewsets.ModelViewSet):
    queryset = Especialidad.objects.all()
    serializer_class = EspecialidadSerializer
    permission_classes = [permissions.IsAuthenticated]


class MedicosEspecialidadViewSet(viewsets.ModelViewSet):
    queryset = MedicosEspecialidad.objects.all()
    serializer_class = MedicosEspecialidadesSerializer
    permission_classes = [permissions.IsAuthenticated]


class HorarioViewSet(viewsets.ModelViewSet):
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
    permission_classes = [permissions.IsAuthenticated]


class CitaViewSet(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer
    permission_classes = [permissions.IsAuthenticated]


