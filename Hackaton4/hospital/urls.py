from rest_framework import routers

from hospital.views import MedicoViewSet, PacienteViewSet, EspecialidadViewSet, MedicosEspecialidadViewSet, \
    HorarioViewSet, CitaViewSet

router = routers.DefaultRouter()
router.register(r'medico', MedicoViewSet)
router.register(r'paciente', PacienteViewSet)
router.register(r'especialidad', EspecialidadViewSet)
router.register(r'medicosEspecialidad', MedicosEspecialidadViewSet)
router.register(r'horarios', HorarioViewSet)
router.register(r'cita', CitaViewSet)
urlpatterns = [

]

