from rest_framework import serializers

from hospital.models import Medico, Paciente, Especialidad, MedicosEspecialidad, Horario


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = ['id','nombres', 'apellidos', 'dni', 'direccion', 'correo', 'telefono', 'sexo', 'num_colegiatura', 'fecha_nacimiento', 'activo']


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = ['id','nombres', 'apellidos', 'dni', 'direccion', 'telefono', 'sexo', 'fecha_nacimiento', 'activo']


class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidad
        fields = ['id','nombres', 'descripcion', 'activo']


class MedicosEspecialidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialidad
        fields = ['id','medico', 'especialidad', 'activo']


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = ['id','medico', 'fecha_atencion', 'inicio_atencion', 'fin_atencion', 'activo']


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = ['id','medico', 'paciente', 'fecha_atencion', 'inicio_atencion','fin_atencion', 'estado', 'observaciones', 'activo']



