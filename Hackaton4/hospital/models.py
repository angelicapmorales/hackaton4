from django.db import models



# Create your models here.

class CommonInfo(models.Model):
    fecha_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.CharField(max_length=100)
    usuario_modificacion = models.CharField(max_length=100)

    class Meta:
        abstract = True


class Medico(CommonInfo):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    dni = models.CharField(max_length=15)
    direccion = models.CharField(max_length=100)
    correo = models.EmailField()
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=5)
    num_colegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()
    activo = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.nombres} {self.apellidos}'

class Paciente(CommonInfo):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    dni = models.CharField(max_length=15)
    direccion = models.CharField(max_length=100)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=5)
    fecha_nacimiento = models.DateField()
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.nombres} {self.apellidos}'


class Especialidad(CommonInfo):
   nombre = models.CharField(max_length=100)
   descripcion = models.TextField()
   activo = models.BooleanField()

   def __str__(self):
       return f'{self.nombre}'

class MedicosEspecialidad(CommonInfo):
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.medico} {self.especialidad}'

class Horario(CommonInfo):
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion=models.DateField()
    inicio_atencion=models.TimeField()
    fin_atencion=models.TimeField()
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.fecha_atencion}'

class Cita(CommonInfo):
     medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
     paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
     fecha_atencion=models.DateField()
     inicio_atencion=models.DateTimeField()
     fin_atencion=models.DateTimeField()
     estado=models.CharField(max_length=15)
     observaciones=models.TextField()
     activo = models.BooleanField()

     def __str__(self):
         return f'{self.fecha_atencion}'
